# Copyright 2024 Infleqtion
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from dataclasses import dataclass, fields
import importlib
import os
import requests
import subprocess
import sys

import click
import papermill
from trogon import tui


@dataclass
class AuthenticationCredentials:
    domain: str
    username: str
    password: str
    audience: str
    client_id: str
    client_secret: str


@dataclass
class Notebook:
    directory: str
    name: str


def authenticate(credentials: AuthenticationCredentials) -> None:
    """Authenticate with auth0 using the provided credentials. Once authenticated writes
    the access token to file
    Args:
        credentials (AuthenticationCredentials): The credentials to authenticate with
    """

    bad_credentials = []
    for credential in fields(credentials):
        if not getattr(credentials, credential.name):
            bad_credentials.append(credential.name)
    if bad_credentials:
        raise ValueError(
            f"The following credential values are missing: {bad_credentials}"
        )

    try:
        response = requests.post(
            f"https://{credentials.domain}/oauth/token",
            data={
                "grant_type": "password",
                "username": credentials.username,
                "password": credentials.password,
                "audience": credentials.audience,
                "scope": "openid profile email offline_access",
                "client_id": credentials.client_id,
                "client_secret": credentials.client_secret,
            },
        )
        response.raise_for_status()
    except Exception as err:
        print(f"Failed to request access token: {err}")
        sys.exit()

    try:
        access_token = response.json()["access_token"]
    except Exception as err:
        print(f"Failed to parse access token: {err}")
        sys.exit()

    with open(os.path.join(os.path.expanduser("~"), ".access-token.txt"), "w+") as file:
        file.write(access_token)


def get_notebooks(notebook: str | None = None) -> list[Notebook]:
    """Get all of the jupyter notebooks inside the project
    Returns:
        notebooks (list[Notebook]): The list of notebooks in the project
    """
    notebooks = []
    for root, _, files in os.walk("oqtant_documentation"):
        for file in files:
            if notebook and file == notebook:
                notebooks = [Notebook(directory=root, name=file)]
                break
            elif not notebook and file.endswith(".ipynb"):
                notebooks.append(Notebook(directory=root, name=file))
    return notebooks


def run_notebooks(notebooks: list[str]) -> list[str]:
    """Run the jupyter notebooks with papermill
    Args:
        notebooks (list[Notebook]): The list of notebooks to run
    Returns: errors (list[str]): List of the names of any notebooks that failed to run
    """
    errors = []
    for notebook in notebooks:
        original_file = os.path.join(notebook.directory, notebook.name)
        temp_file = f"{original_file.replace('.ipynb', '')}-output.ipynb"
        try:
            print(f"Running notebook: {notebook.name}")
            papermill.execute_notebook(original_file, temp_file)
        except Exception as err:
            print(f"An error occurred running notebook ({notebook.name}): {err}")
            errors.append(notebook.name)
    return errors


def build_notebooks() -> None:
    """Run the mkdocs build function to run the notebook(s) and generate the documentation site files"""

    try:
        subprocess.run(
            [
                "mkdocs",
                "build",
                "--config-file",
                "oqtant_documentation/mkdocs.yml",
            ]
        )
    except Exception as err:
        print(f"An error occurred building the notebooks: {err}")


def cleanup_output_notebooks() -> None:
    """Clean up the temp files created by papermill"""
    for root, _, files in os.walk("oqtant_documentation"):
        for file in files:
            if "output" in file and file.endswith(".ipynb"):
                os.rename(
                    os.path.join(root, file),
                    os.path.join(root, file.replace("-output", "")),
                )


@tui()
@click.command()
@click.option(
    "--domain",
    "-d",
    required=False,
    type=str,
    default=os.getenv("AUTH0_DOMAIN"),
    help="The auth0 domain to authenticate with",
)
@click.option(
    "--username",
    "-u",
    required=False,
    type=str,
    default=os.getenv("AUTH0_USERNAME"),
    help="The username of the account to authenticate with",
)
@click.option(
    "--password",
    "-p",
    required=False,
    type=str,
    default=os.getenv("AUTH0_PASSWORD"),
    help="The password of the account to authenticate with",
)
@click.option(
    "--audience",
    "-a",
    required=False,
    type=str,
    default=os.getenv("AUTH0_AUDIENCE"),
    help="The auth0 audience to authenticate with",
)
@click.option(
    "--client_id",
    "-c",
    required=False,
    type=str,
    default=os.getenv("AUTH0_CLIENT_ID"),
    help="The auth0 client id to authenticate with",
)
@click.option(
    "--client_secret",
    "-s",
    required=False,
    type=str,
    default=os.getenv("AUTH0_CLIENT_SECRET"),
    help="The auth0 client secret to authenticate with",
)
@click.option(
    "--notebook",
    "-n",
    required=False,
    type=str,
    default=os.getenv("NOTEBERT_NOTEBOOK"),
    help="(Optional) The notebook to run specifically",
)
def run(
    domain: str | None,
    username: str | None,
    password: str | None,
    audience: str | None,
    client_id: str | None,
    client_secret: str | None,
    notebook: str | None,
) -> None:
    """Runner to run jupyter notebooks and generate mkdocs site files
    Args:
        domain (str | None): The auth0 domain to authenticate with
        username (str | None): The auth0 username to authenticate with
        password (str | None): The auth0 password to authenticate with
        audience (str | None): The auth0 audience to authenticate with
        client_id (str | None): The auth0 client id to authenticate with
        client_secret (str | None): The auth0 client secret to authenticate with
        notebook (str | None): The file path to a specific notebook to run
    """

    def print_version(module: str, bg_color: str = "cyan") -> None:
        """Print the version of the provided module in a 'cute' way
        Args:
            module (str): The name of the module to print
            bg_color (str = "cyan"): Optional, the background color to print with
        """
        version = f" {module} v{importlib.metadata.version(module)} "
        click.secho(
            f"{version:*^80}",
            bg=bg_color,
            fg="black",
        )

    print_version("oqtant-documentation")
    print_version("oqtant", bg_color="green")
    print_version("bert-schemas", bg_color="yellow")

    credentials = AuthenticationCredentials(
        domain=domain,
        username=username,
        password=password,
        audience=audience,
        client_id=client_id,
        client_secret=client_secret,
    )
    authenticate(credentials)

    notebooks = get_notebooks(notebook=notebook)
    errors = run_notebooks(notebooks=notebooks)

    if not errors:
        cleanup_output_notebooks()
        build_notebooks()
        print(f"Process successfully ran and built {len(notebooks)} notebook(s)")
    else:
        print("Processing failed to complete successfully")
        print(f"The following notebooks encountered errors: {errors}")
        sys.exit()

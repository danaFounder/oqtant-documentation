## v0.1.0 (2024-02-05)

### added (1 change)

- [Changelog + Goodies](coldquanta/albert/oqtant-documentation@9d7d75bfe12fdad93f8a13bb47c4e0d4c8796783) ([merge request](coldquanta/albert/oqtant-documentation!1))
## 0.5.0 (2024-05-13)

### fixed (3 changes)

- [Fixing print statement in Walkthrough 1](coldquanta/albert/oqtant-documentation@dad754997116c568a4b7e180b8257bc95ce88b87) ([merge request](coldquanta/albert/oqtant-documentation!31))
- [Fixing walkthroughs](coldquanta/albert/oqtant-documentation@f7afd5de666591ccc750ddd0dd6f259d661f6d1e) ([merge request](coldquanta/albert/oqtant-documentation!30))
- [Fixed remaining bugs and typos in demos 3 and 4](coldquanta/albert/oqtant-documentation@3abec6badf33135376e1f0636eb8333a6e37cc06) ([merge request](coldquanta/albert/oqtant-documentation!29))

### updated (1 change)

- [Updating notebook instructions for loggin in](coldquanta/albert/oqtant-documentation@667179f6b22c320783cc8e118bbc76204f69de65) ([merge request](coldquanta/albert/oqtant-documentation!28))

### added|removed|updated|fixed (1 change)

- [Branch with demo 5 (solitons)](coldquanta/albert/oqtant-documentation@e80c50a384ccbdd5e4fb5b97d52d4625debda316) ([merge request](coldquanta/albert/oqtant-documentation!24))

## 0.4.0 (2024-04-29)

### added (7 changes)

- [Terminator walkthrough](coldquanta/albert/oqtant-documentation@625c13d69c8319985c44cdf425d902af1c6230c4) ([merge request](coldquanta/albert/oqtant-documentation!21))
- [Adding load job from id to Walkthrough 1](coldquanta/albert/oqtant-documentation@fded08a56afa6ac87462d928a520c4de3ae8542e) ([merge request](coldquanta/albert/oqtant-documentation!26))
- [changed to use notebook_starter](coldquanta/albert/oqtant-documentation@e0f08602d260334b2863ea33f824523fdeec78ce) ([merge request](coldquanta/albert/oqtant-documentation!13))
- [ALB-6369: Notebert](coldquanta/albert/oqtant-documentation@6d6648d84af3a1c04e29fa219bf2eea9e4e90b19) by @mc.gruff ([merge request](coldquanta/albert/oqtant-documentation!10))
- [Demos walkthroughs 240214](coldquanta/albert/oqtant-documentation@838a2fa641d52e3613b39301e13591ddd17bf142) ([merge request](coldquanta/albert/oqtant-documentation!8))
- [More Improvements](coldquanta/albert/oqtant-documentation@b53f2fafaa062beee7f049f27ef5690dcef9ef74) ([merge request](coldquanta/albert/oqtant-documentation!6))
- [Math](coldquanta/albert/oqtant-documentation@4da3875afb5bc4a87a04d4bc63d41a331ee656a0) ([merge request](coldquanta/albert/oqtant-documentation!4))

### added|removed|updated|fixed (8 changes)

- [fix simulator grid problems](coldquanta/albert/oqtant-documentation@48ef4c7d4072648a25f6ae3694e583b1013effe5) ([merge request](coldquanta/albert/oqtant-documentation!27))
- [Removed links](coldquanta/albert/oqtant-documentation@3d1fa3414014e18d0dcee16a73bdd74ee222fd20) ([merge request](coldquanta/albert/oqtant-documentation!22))
- [Temp change demo 1](coldquanta/albert/oqtant-documentation@d7ed0ae95f2dec27cbd0f56e450342a60d3d174a) ([merge request](coldquanta/albert/oqtant-documentation!17))
- [small text changes](coldquanta/albert/oqtant-documentation@6019aa4e2c692c9e30ed1c4a777abba9533b6c1a) ([merge request](coldquanta/albert/oqtant-documentation!15))
- [Demo 4 speed of sound](coldquanta/albert/oqtant-documentation@68a02eae4fcdbc23fef8537f76f0054cc28a9d14) ([merge request](coldquanta/albert/oqtant-documentation!14))
- [adding gitlab links and job run totals to all example notebooks](coldquanta/albert/oqtant-documentation@e266d81dccce8c60d2fe72018c875c7de601a447) ([merge request](coldquanta/albert/oqtant-documentation!11))
- [demo 3](coldquanta/albert/oqtant-documentation@0a0c12ac2cdfe37fb5b1525bc837caf528bd2613) ([merge request](coldquanta/albert/oqtant-documentation!7))
- [testing gitlab pages](coldquanta/albert/oqtant-documentation@76ad2ee807b1da6feb39ef49b4184a35ad952af9) ([merge request](coldquanta/albert/oqtant-documentation!2))

### updated (7 changes)

- [updated README and added a dev settings file](coldquanta/albert/oqtant-documentation@64cd65f49f08aa90f7168197046ac92302198ca4) ([merge request](coldquanta/albert/oqtant-documentation!25))
- [ALB-6376: Batch Jobs Result Handling](coldquanta/albert/oqtant-documentation@f17d386bad2fc58f91baabb3d1b0ca499a1ffdbb) by @mc.gruff ([merge request](coldquanta/albert/oqtant-documentation!12))
- [added notebook download buttons](coldquanta/albert/oqtant-documentation@d2e9f15950868bb07852d7c6e28bfa71df42d591) ([merge request](coldquanta/albert/oqtant-documentation!23))
- [Documentation homepage](coldquanta/albert/oqtant-documentation@54b28c1319b125aba399d7492f0730cb678855c7) ([merge request](coldquanta/albert/oqtant-documentation!19))
- [Change example barrier parameters to match what is in the supporting text.](coldquanta/albert/oqtant-documentation@ce08f1fbbbe69bc400bcd8cd23802edf834ffe8a) ([merge request](coldquanta/albert/oqtant-documentation!18))
- [Demos walkthroughs 240214](coldquanta/albert/oqtant-documentation@7c8e9c7014536f9961c14b6b30b57d5e21caf281) ([merge request](coldquanta/albert/oqtant-documentation!9))
- [mkdocs improvements to homepage, images, layout](coldquanta/albert/oqtant-documentation@0658ed3a934a4d22fbd1754d52ea8654497b58c2) ([merge request](coldquanta/albert/oqtant-documentation!5))

### fixed (1 change)

- [rm <path to virtual environment location> from install instructions](coldquanta/albert/oqtant-documentation@8f2d0a781e07a921ee5d8dd2fb64d4a2f01cf545) ([merge request](coldquanta/albert/oqtant-documentation!3))

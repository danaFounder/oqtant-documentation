---
hide:
  - navigation
---

# Welcome to Oqtant Documentation

<div markdown="1" class="card-list">

<div markdown="1" class="card" id="quickstart" onclick="location.href='INSTALL';">
<div markdown="1" class="icon-outer">
<div class="icon">
<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
  <path stroke-linecap="round" stroke-linejoin="round" d="m3.75 13.5 10.5-11.25L12 10.5h8.25L9.75 21.75 12 13.5H3.75Z" />
</svg>

</div>
</div>
<div markdown="span" class="card-title">Quickstart Guide</div>
<div markdown="span" class="bg"></div>
</div>

<div markdown="1" class="card" onclick="location.href='oqtant_client_docs';">
<div markdown="1" class="icon-outer">
<div class="icon">
<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
  <path stroke-linecap="round" stroke-linejoin="round" d="m6.75 7.5 3 2.25-3 2.25m4.5 0h3m-9 8.25h13.5A2.25 2.25 0 0 0 21 18V6a2.25 2.25 0 0 0-2.25-2.25H5.25A2.25 2.25 0 0 0 3 6v12a2.25 2.25 0 0 0 2.25 2.25Z" />
</svg>
</div>
</div>
<div markdown="span" class="card-title">Oqtant API</div>
<div markdown="span" class="bg"></div>
</div>

<div markdown="1" class="card" onclick="location.href='oqtant_rest_api_docs';">
<div markdown="1" class="icon-outer">
<div class="icon">
<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" >
  <path stroke-linecap="round" stroke-linejoin="round" d="M2.25 15a4.5 4.5 0 0 0 4.5 4.5H18a3.75 3.75 0 0 0 1.332-7.257 3 3 0 0 0-3.758-3.848 5.25 5.25 0 0 0-10.233 2.33A4.502 4.502 0 0 0 2.25 15Z" />
</svg>

</div>
</div>
<div markdown="span" class="card-title">Oqtant REST API</div>
<div markdown="span" class="bg"></div>
</div>
<div markdown="1" class="card" onclick="location.href='examples/hello_world';">

<div markdown="1" class="icon-outer">
<div class="icon">
<svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor">
  <path stroke-linecap="round" stroke-linejoin="round" d="M15.59 14.37a6 6 0 0 1-5.84 7.38v-4.8m5.84-2.58a14.98 14.98 0 0 0 6.16-12.12A14.98 14.98 0 0 0 9.631 8.41m5.96 5.96a14.926 14.926 0 0 1-5.841 2.58m-.119-8.54a6 6 0 0 0-7.381 5.84h4.8m2.581-5.84a14.927 14.927 0 0 0-2.58 5.84m2.699 2.7c-.103.021-.207.041-.311.06a15.09 15.09 0 0 1-2.448-2.448 14.9 14.9 0 0 1 .06-.312m-2.24 2.39a4.493 4.493 0 0 0-1.757 4.306 4.493 4.493 0 0 0 4.306-1.758M16.5 9a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0Z" />
</svg>
</div>
</div>
<div markdown="span" class="card-title">Example Notebooks</div>
<div markdown="span" class="bg"></div>
</div>

</div>

## About Oqtant API (OqtAPI)

Oqtant API (OqtAPI) is a Python package developed by Infleqtion that provides an expanded set of tools for interfacing with Oqtant hardware. From more fine-tuned control over job inputs to in depth data analysis tooling, Oqtant puts more power in the hands of the user.

## What you can do

- Interface with Oqtant using Python
- Start with example Jupyter notebooks
- Program time-varying arbitrary optical fields that shape and split your Bose-Einstein condensate (BEC)
- Study experimental topics such as interference, coherence, superfluidity, and nonlinear phenomena
- Access all the functionality of the Oqtant Web App
- Submit a single job, or batch of jobs for sequential processing
- Monitor the status of your submitted jobs and retrieve results when they are complete
- Analyze results using the provided tools or your own, the power is yours!

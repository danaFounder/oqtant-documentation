# Oqtant Documentation

## Installation

To install this project, clone the repository from gitlab using the following command:

```shell
git clone git@gitlab.com:coldquanta/albert/oqtant-documentation.git
```

### Set up Poetry

Provided that you have Python 3.10 or better installed, your next step is to install Poetry, if not already installed.

```shell
curl -sSL https://install.python-poetry.org | python3 -
```

More info on Poetry is available here: https://python-poetry.org/docs/

```shell
poetry config virtualenvs.create false
python3 -m venv .venv
```

The Poetry shell is activated via:

```shell
poetry shell
```

### Setup Repo

This project is will need access to the latest versions of OqtAPI even if they are not currently published publicly on PyPI. In order to be able to find the latest versions published in our internal repository poetry needs to be configured to access it.

A personal GitLab access token will be required to accomplish this. Follow the steps here: https://gitlab.com/-/profile/personal_access_tokens

When you create a personal access token you will only have access to it in GitLab when its created, it is recommended you store this token in a safe place as you will need to use it later.

### Gitlab Registry

In order for poetry to know where to look for OqtAPI versions the following will be required to configure the bert-packages repository in your poetry config.

```bash
poetry config repositories.bert-packages https://gitlab.com/api/v4/projects/39544653/packages/pypi
poetry config pypi-token.bert-packages <personal access token>
poetry config http-basic.bert-packages __token__ <personal access token>
```

### Install Requirements

```shell
poetry install
```

### Setup Pre-Commit

This project uses pre-commit for maintaining code uniformity and quality. Once you have the requirements installed from poetry run the following command to get pre-commit configured:

```shell
pre-commit install
pre-commit install --hook-type pre-push
```

### Adding Notebooks or Pages

New markdown files or notebooks will need to be added to the navigation structure in `mkdocs.yml`

See examples here: https://www.mkdocs.org/user-guide/writing-your-docs/#configure-pages-and-navigation

### Running Notebooks against Development Environment

To run a notebook against the development environment you will need to override the environment variables which oqtant uses to determine which servers to connect to. To do this you will need to edit the "settings-dev.env" file, adding in your
username and password and the AUTH0_CLIENT_SECRET, which can be found in Keeper. Then run `source settings-dev.env` in your terminal.

!! IMPORTANT: DO NOT COMMIT YOUR CHANGES TO THIS FILE.

### Notebert

Notebert is a script that can be used to automatically run one or all of the notebooks located inside `oqtant_documentation`. A number of arguments are required to bypass the default notebook authentication process, these arguments are:

- `--domain`: the domain of the auth0 instance to authenticate against
- `--username`: the email of the account you wish to run the notebooks with
- `--password`: the password of the account you wish to run the notebooks with
- `--audience`: the audience of the auth0 instance to authenticate with
- `--client_id`: the client id of the auth0 application to authenticate with
- `--client_secret`: the client secret of the auth0 application to authenticate with

The optional `--notebook` argument can be used to target a single notebook to run. The value of this argument should be the full name of the notebook file (this includes the '.ipynb' extension)

Notebert can be run in a couple different ways:

- Pure CLI (`notebert run`)
- CLI with GUI (`notebert tui`)

Both of these options can take advantage of environment variables to avoid having to specify arguments each time the script is called. These environment variables are:

- AUTH0_DOMAIN == `--domain`
- AUTH0_USERNAME == `--username`
- AUTH0_PASSWORD == `--password`
- AUTH0_AUDIENCE == `--audience`
- AUTH0_CLIENT_ID == `--client_id`
- AUTH0_CLIENT_SECRET == `--client_secret`
- NOTEBERT_NOTEBOOK == `--notebook`


When `poetry install` is run inside of either `poetry shell` or a sourced virtual environment you will have access to the `notebert` script. A default `settings.env` and `settings.bat` file is included in this project that contains settings for the environment variable arguments. The secret values have been removed and will need to be updated in order for things to work. Remember to NEVER commit changes that include secrets in this file.

Clicking on "Close & Run" or hitting "Ctrl+R" will start the process. "Ctrl+C" will exit the script.

## Changelog process

We use an automated changelog process within the Gitlab repository. There are two aspects of this process. First is the changelog verb in the squash commit and the second is the tagging process.

### Requirements

You need to have an approved, but not yet merged, pull request with your changes from a feature branch.

### Gitlab UI Steps

1. Click on the “Edit commit message” checkbox inside the merge request

   ![Example for Edit Commit Message](/misc/merge1-example.png)

2. Update the Squash Commit message by adding a useful message

   ![Example for Squish Commit Message](/misc/merge2-example.png)

3. Update the "Changelog: added|removed|updated|fixed" section by deleting all other changelog options other than what your change produced.

   Example:If your change is adding functionality you should update this section to be "Changelog: added"
   ![Final Merge Example](/misc/merge3-example.png)

4. Click Merge

### CMD line git Steps

For changes that are done directly to develop from local, or manual squash merges, you add `--trailer "Changelog: <verb>"` to the end of the commit command

### Tagging process

Now that the changes are in we can generate the CHANGELOG.md for the first time. Navigate to the tags section of your repository and create a tag (we use semantic versioning so each new tag should be at least one fix version above the previous). After creating the tag you can navigate to the CI/CD section of your repository and see a new pipeline running titled “changelog”. Monitor it until finished (successfully) and now check the default branch in your repository, you should see a new commit from your Project Access Token that contains the new CHANGELOG.md!

### Useful links

[Link to Automatic Changelog Demo](https://coldquanta.atlassian.net/l/cp/cfx5d1oq)

[Link to Gitlab Changelog Entries](https://docs.gitlab.com/ee/development/changelog.html)

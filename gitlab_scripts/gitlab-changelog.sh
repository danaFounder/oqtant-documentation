#!/bin/bash

GITLAB_PROJECT_ACCESS_TOKEN=$1
GITLAB_PROJECT_URL=$2
GITLAB_PROJECT_ID=$3
GITLAB_TARGET_BRANCH=$4
GITLAB_TAG_VERSION=$5

CHANGELOG_LINK=${GITLAB_PROJECT_URL}/-/blob/${GITLAB_TARGET_BRANCH}/CHANGELOG.md

GITLAB_PROJECT_CHANGELOG_API_URL=https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/repository/changelog
GITLAB_PROJECT_TAG_API_URL=https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/repository/tags

GITLAB_EXISTING_TAG_VERSION=$(curl --request GET --header "PRIVATE-TOKEN: ${GITLAB_PROJECT_ACCESS_TOKEN}" "${GITLAB_PROJECT_TAG_API_URL}" | jq -r '.[1] | .name')

if [ -z "$GITLAB_EXISTING_TAG_VERSION" ]; then
    echo "Could not find an existing tag"
    exit 1
else
    echo "Found existing tag ${GITLAB_EXISTING_TAG_VERSION}"
fi

RESPONSE=$(curl --request POST --header "PRIVATE-TOKEN: ${GITLAB_PROJECT_ACCESS_TOKEN}" --data "version=${GITLAB_TAG_VERSION}&from=${GITLAB_EXISTING_TAG_VERSION}&branch=${GITLAB_TARGET_BRANCH}" "${GITLAB_PROJECT_CHANGELOG_API_URL}")

if [ $RESPONSE == 200 ]; then
    echo "Updated changelog: ${CHANGELOG_LINK}"
else
    echo $RESPONSE
fi
